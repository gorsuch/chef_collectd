#
# Cookbook Name:: collectd
# Attributes:: default
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ubuntu_precise =
  (node[:platform].eql?('ubuntu') && node[:platform_version] >= '12.04')
debian_squeeze =
  (node[:platform].eql?('debian') && node[:platform_version] >= '7.0')

if node[:kernel][:machine].eql?('x86_64') && !ubuntu_precise && !debian_squeeze
  plugin_dir = '/usr/lib64/collectd'
else
  plugin_dir = '/usr/lib/collectd'
end

collectd_default_attributes = {
  interval: 10,
  read_threads: 5,
  version: '5.4.0',
  fqdn_lookup: 'false',
  server_role: 'collectd-server',
  log_level: 'error',
  log_file: 'collectd.log',
  log_timestamp: 'true',
  log_print_severity: 'false',
  ulimit: {
    file_descriptors: '65536'
  },
  autoconf_opts: nil,
  graphite: {
    server: '127.0.0.1',
    port: 2003,
    prefix: 'collectd.',
    escape_character: '_',
    store_rates: true
  },
  prefix_dir: '/usr',
  sysconf_dir: '/etc/collectd',
  plugconf_dir: plugin_dir,
  bin_dir: '/usr/bin',
  sbin_dir: '/usr/sbin',
  log_dir: '/var/log/collectd/',
  types_db: '/usr/share/collectd/types.db',
  src_dir: '/opt/src-collectd'
}

node.default[:collectd].merge!(collectd_default_attributes)
